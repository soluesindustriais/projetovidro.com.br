<?php
include('inc/vetKey.php');
$h1 = "porta de vidro temperado";
$title = $h1;
$desc = "Porta de vidro temperado: Veja aqui!  ";
$key = "porta,de,vidro,temperado";
$legendaImagem = "Foto ilustrativa de porta de vidro temperado";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=$url?>assets/css/mpi-style.css">
</head>

<body>
    <?php include 'inc/header.php' ?>   
        <div class="container">
            <div class="row">
                <article class="col-md-9 col-12">
                    <?php $quantia = 3;
                    include('inc/gallery.php'); ?>

                    <h2>Porta de vidro temperado: Saiba tudo aqui! </h2><p>As portas de vidro se encaixam harmoniosamente nos requisitos modernos de arquitetura e design. Elas se tornaram populares não apenas em escritórios, lojas e instalações de hotéis, mas também em ambientes domésticos como casas e apartamentos. Estas portas dão a elegância na casa, e tornam o ambiente mais leve e com melhor visibilidade, além da praticidade e funcionalidade.</p><h2>Benefícios da porta de vidro temperado</h2><p>O vidro temperado proporciona diversos benefícios, além de ser super resistente, ele é considerado um vidro de segurança. Essa característica é decorrente de seu processo de tratamento conhecido como tempera, onde o vidro  é gradualmente aquecido a uma temperatura de 600 graus e, em seguida, o vidro é rapidamente arrefecido. Isso permite aumentar a resistência ao calor, a durabilidade e a segurança do vidro.</p><p>O vidro temperado pode suportar cargas 5 vezes mais que o vidro comum. Esta propriedade possibilitou a utilização de vidro temperado na fabricação de portas, tampos de mesa, degraus entre diversas outras finalidades. Em suma, o mais indicado para portas é o vidro temperado, porque conta com benefícios como:</p><ul><li>Alta segurança;</li><li>Longa durabilidade;</li><li>Resistência a impactos;</li><li>Resistência a umidade;</li><li>Funcionalidade;</li><li>Modernidade;</li><li>Ação antibacteriana.</li></ul><h2>Tipos de porta de vidro </h2><p>Existem vários tipos de porta de vidro temperado, com diferentes maneiras de manuseio. Algumas com mais funcionalidade que outras, e também, podendo ser manual ou automática. O mais importante é escolher aquela que se adéqua ao espaço, e que seja mais ideal para tornar o ambiente mais prático e funcional. Veja a seguir quais os tipos: </p><h3>Portas de vidro basculantes</h3><p>A porta de vidro temperado basculante é a mais comum,  abre em uma única direção e retorna facilmente à sua posição original. Elas são instaladas tanto em entradas de casas e apartamentos, como em escritórios e todos os demais pontos comerciais. Ou seja, é aquele tipo de porta bem versátil, mas ao mesmo tempo, moderna e elegante, pois o vidro adiciona essas características em qualquer ambiente.</p><h3>Portas de vidro deslizante</h3><p>Esse tipo de porta de vidro temperado deslizante se move de uma lado para outro, e é ideal para ambientes pequenos, ou para quem quer otimizar o espaço.  Essas portas podem executar tanto a função de  portas de entrada, como para espaços internos. Além disso, esse tipo de porta é bastante versátil, e instalam-se em escritórios, hotéis, lojas, centros comerciais, casas e apartamentos.</p><h3></h3><h3>Portas de vidro giratória</h3><p>As portas de vidro giratórias abrem em uma direção, fazem o giro de 360º e retornam ao centro.  Elas são instaladas em bancos, escritórios, hospitais.... entre outros pontos comerciais. Além disso, são confortáveis ​​e funcionais e proporcionam bastante praticidade no ambiente, sendo portanto, ideal para lugares com grande fluxo de pessoas. </p><h3>Portas de vidro pivotante</h3><p>As portas de vidro pivotantes são caracterizadas pela estética e funcionalidade. Elas são bem amplas e proporcionam maior visibilidade e aspecto limpo no ambiente, sendo bastante utilizadas tanto em ambientes domésticos como em escritórios, grandes prédios, e pontos comerciais como shoppings. </p>

                    </article>
                <?php include('inc/coluna-lateral.php'); ?>
                <br class="clear" />
                <?php include('inc/paginas-relacionadas.php'); ?>
                <?php include('inc/regioes.php'); ?>
                <br class="clear">
                <?php include('inc/copyright.php'); ?>
            </div>
        </div>    
    <?php include('inc/footer.php'); ?>
</body>
</html>