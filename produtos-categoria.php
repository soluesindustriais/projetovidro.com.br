<? $h1 = "Categoria - Produtos";
$title  = "Categoria - Produtos";
$desc = "Orce $h1, conheça os melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include('inc/head.php'); ?>
<link rel="stylesheet" href="<?= $url ?>assets/css/thumbnails.css">
<style>
    <? include('assets/css/style-lista.css');
    ?>
</style>
</head>

<body>
    <? include('inc/topo.php'); ?>
    <div class="wrapper">
        <main role="main">
            <div class="content">
                <div class="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                            <nav aria-label="breadcrumb">
                                <ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                        <a href="' . $url . '" itemprop="item" title="Home">
                                            <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home » </span>
                                        </a>
                                        <meta itemprop="position" content="1" />
                                    </li>
                                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                        <span itemprop="name">Produtos</span>
                                        <meta itemprop="position" content="2" />
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <section> <?php include_once('inc/produtos/produtos-buscas-relacionadas.php'); ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article class="full">
                        <p>O mercado de <?= $h1 ?> é amplo e conta com produtos e serviços que podem ser úteis em diversas
                            aplicações. No Soluções Industriais, portal especializado na geração de negócios para o
                            mercado B2B, é possível encontrar as melhores empresas que atuam nesse segmento.</p>
                        <p>Além de receber um orçamento, você também poderá esclarecer suas dúvidas referentes ao
                            assunto. Saiba mais sobre <?= $h1 ?> e faça uma cotação.</p>
                        <ul class="thumbnails-2"> <?php include_once('inc/produtos/produtos-categoria.php'); ?> </ul>
                    </article> <br class="clear">
                </section>
            </div>
        </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php'); ?>
</body>

</html>