<?php
include('inc/vetKey.php');
$h1 = "box acrílico";
$title = $h1;
$desc = "Quais são as características de um box acrílico? Ótima opção para deixar o banheiro mais moderno, prático e bonito, o box acrílico tem a estrutura de";
$key = "box,acrílico";
$legendaImagem = "Foto ilustrativa de box acrílico";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=$url?>assets/css/mpi-style.css">
</head>

<body>
    <?php include 'inc/header.php' ?>   
        <div class="container">
            <div class="row">
                <article class="col-md-9 col-12">
                    <?php $quantia = 3;
                    include('inc/gallery.php'); ?>

                    <h2>Quais são as características de um box acrílico?</h2><p>Ótima opção para deixar o banheiro mais moderno, prático e bonito, o box acrílico tem a estrutura de alumínio e é feito com uma chapa de 3 mm de qualquer tipo de plástico. O modelo tradicional tem o perfil arredondado e com trilho inferior autolimpante para facilitar a limpeza. Ele pode ser fabricado na cor e modelo da sua escolha, de modo que essa aquisição possa combinar com o resto da decoração do seu banheiro.</p><h2>Como higienizar um box acrílico?</h2><p>Embora possa não parecer, a manutenção do box acrílico é bem simples. Para higienizar a sua superfície, basta usar um pano de algodão limpo e sem fiapos embebido em uma mistura de água e sabão ou detergente neutro e depois enxaguar para tirar os resquícios dessa composição da peça. Caso tenha alguma mancha mais resistente no seu box acrílico, dilua uma xícara de amaciante em água morna e passe na peça para tirar qualquer marca de sabonete ou gordura. Existem alguns produtos cujo uso estão expressamente proibidos em materiais desse tipo, que são:</p><ul><li>Esponjas abrasivas;</li><li>Palha de aço;</li><li>Solventes minerais;</li><li>Álcool;</li><li>Querosene;</li><li>Thinner.</li></ul><h2>Saiba quais cuidados deve ter com um box acrílico</h2><p>Uma das principais vantagens do box acrílico e que faz com que muitas pessoas optem por esse material é o preço: em média, ele é 50% mais barato que o de vidro, mesmo sendo menos resistente e durável. Entretanto, se certos cuidados não forem tomados, o barato pode sair caro. Por isso, embora a higienização do material seja mais simples, é preciso fazê-la periodicamente para que a peça não crie mofo, principalmente no verão quando a umidade do ar é mais alta.</p><p>Além disso, o box acrílico se deteriora mais rápido que o box de vidro, tornando essencial que esse tipo de material tenha um suporte de alumínio que seja resistente a ferrugem, aumentando a vida útil do elemento no seu banheiro. Vale ressaltar que caso o box acrílico sofra um impacto muito forte e se quebre, é necessário trocar a peça imediatamente para evitar acidentes com partes cortantes do objeto.</p>

                    </article>
                <?php include('inc/coluna-lateral.php'); ?>
                <br class="clear" />
                <?php include('inc/paginas-relacionadas.php'); ?>
                <?php include('inc/regioes.php'); ?>
                <br class="clear">
                <?php include('inc/copyright.php'); ?>
            </div>
        </div>    
    <?php include('inc/footer.php'); ?>
</body>
</html>