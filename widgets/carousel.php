<div id="carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators lineSlide">
		<li data-target="#carousel" data-slide-to="0" class="active"></li>
		<li data-target="#carousel" data-slide-to="1"></li>
		<li data-target="#carousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="carousel-item active one">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo" style="">box blindex</h2>
				<p class="sdesc" style="">Resistência e impermeabilidade com grandes inovações</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."box-blindex.php")) { echo $url."box-blindex"; } else { echo $url."informacoes"; } ?>" class="btn button-slider2">Saiba Mais</a>
                </div>
			</div>
		</div>	
		<div class="carousel-item three">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo"  style="">Guarda copo</h2>
				<p class="sdesc" style="">Resistência e qualidade</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."guarda-corpo.php")) { echo $url."guarda-corpo"; } else { echo $url."informacoes"; } ?>" class="btn button-slider2">Saiba Mais</a>
			</div>
            </div>
		</div>
        <div class="carousel-item two">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo"  style="">porta de vidro</h2>
				<p class="sdesc"  style="">Segurança e resistência para toda a sua família</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."porta-de-vidro.php")) { echo $url."porta-de-vidro"; } else { echo $url."informacoes"; } ?>" class="btn button-slider2">Saiba Mais</a>
			</div>
            </div>
		</div>
	</div>

	<a class="carousel-control-prev " href="#carousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>

	<a class="carousel-control-next " href="#carousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>

</div>