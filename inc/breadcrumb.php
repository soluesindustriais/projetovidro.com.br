<div class="breadcrumb">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'informacoes" itemprop="item" title="informações">
        <span itemprop="name">informacoes »  </span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
   
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name"> <?= $h1 ?> </span>
      <meta itemprop="position" content="3" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>