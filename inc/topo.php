<header class="sticky-top">
    <nav id="menu" class="navbar navbar-expand-md navbar-light">
        <div class="container" style="padding:0px 15px;width:100%;">
            <a class="navbar-brand" href="<?=$url?>" title="<?=$nomeSite." - ".$slogan?>"><img src="<?=$url?>assets/img/logo.png" alt="<?=$nomeSite." - ".$slogan?>" title="<?=$nomeSite." - ".$slogan?>"></a>
            <button class="navbar-toggler ml-auto mr-3" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation" style="float:right">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar1">
                <ul class="navbar-nav ml-auto">
                    <?php include 'inc/menu.php' ?>
                </ul>
            </div>
        </div>
    </nav>
</header>