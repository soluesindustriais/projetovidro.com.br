<section>
    <div class="container-fluid">
        <div class="row">
          
            <div class="col-12 background-bloco py-5 p-md-5 align-items-center justify-content-center">
                <div class="col-12 col-md-8">               
                    <h2 class="text-destaq text-center">Informações</h2>
                    <p class="sec-p text-dark text-center">O vidro é utilizado em nosso dia a dia. Pode ser aplicado em garrafas de cervejas e refrigerantes, mesas, janelas, recipientes de remédios, espelhos em nossa residência. Por isso, é importante saber a qualidade e segurança do material antes de escolher uma vidraçaria para a sua obra ou reforma, verifique a qualidade do vidro produzido e os tipos de modelos. Escolha sempre por empresas que seus materiais possuem:</p>



                    <div class="row d-flex justify-content-center">

                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/bathroom.png" width="100" alt="Variedade">
                                <p class="mt-3 mb-0">Variedade</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/mirror.png" width="100" alt="Resistência">
                                <p class="mt-3 mb-0">Resistência</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/curtain.png" width="100" alt="Impermeabilidade">
                                <p class="mt-3 mb-0">Impermeabilidade</p>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-2 mb-4">
                            <a href="<?=$url?>informacoes" class="button-slider2">Saiba Mais</a>
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </div>
</section>
