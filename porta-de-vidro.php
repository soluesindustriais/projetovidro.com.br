<?php
include('inc/vetKey.php');
$h1 = "porta de vidro";
$title = $h1;
$desc = "Você conhece os benefícios de uma porta de vidro para o espaço? Bom, se ainda não, você precisa conhecer as muitas vantagens da porta de vidro. Além de otimizar o espaço criando uma visão óptica de amplitude";
$key = "porta,de,vidro";
$legendaImagem = "Foto ilustrativa de porta de vidro";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=$url?>assets/css/mpi-style.css">
</head>

<body>
    <?php include 'inc/header.php' ?>   
        <div class="container">
            <div class="row">
                <article class="col-md-9 col-12">
                    <?php $quantia = 3;
                    include('inc/gallery.php'); ?>

                    <h2>Porta de vidro: quais os benefícios?</h2><p>Você conhece os benefícios de uma porta de vidro para o espaço? Bom, se ainda não, você precisa conhecer as muitas vantagens da porta de vidro. Além de otimizar o espaço criando uma visão óptica de amplitude, as portas de vidro proporcionam modernidade, conforto e leveza para qualquer ambiente. Quer conhecer tudo sobre porta de vidro? Continue lendo o artigo!</p><h2>Qual o tipo de vidro indicado para portas?</h2><p>O vidro temperado é até 3-5 vezes mais forte do que o vidro comum e, portanto, tem uma resistência à tração significativamente maior do que outros tipos de vidro. Isso ocorre porque o vidro temperado é “cozido” e endurecido, dificultando a quebra. Além disso, se quebrar, elas se partem em pedaços muito pequenos, tornando a possibilidade de lesões graves pequenas. Seu uso é feito principalmente em construções especiais. Dessa forma, os benefícios das portas de vidros são extensas, e incluem:</p><ul><li>Alta resistência à impactos;</li><li>Resistente à altas temperaturas;</li><li>Força e durabilidade garantidas;</li><li>Segurança mesmo em caso de quebra;</li><li>Isolamento acústico;</li><li>Melhor visibilidade;</li><li>Modernidade e leveza.</li></ul><h2>Como escolher uma porta de vidro?</h2><p>Para escolher um porta de vidro corretamente, é preciso primeiramente classificar o tipo de projeto pretendido. A porta é para a entrada da casa? Para o escritório? Para uma varanda?</p><p>Ou seja, dependendo de qual seja o tipo de projeto, o design da porta e o tipo de abertura pode variar, isso porque existem certos tipos de portas que se adéquam mais a determinados tipos de ambiente. Outro ponto importante é também atentar-se para o que você deseja, quer mais espaço? As portas de vidro deslizantes são as mais indicadas para criar uma melhor amplitude no ambiente; quer mais praticidade no escritório? As portas automáticas são uma ótima escolha.</p><h2>Critérios para escolher uma vidraçaria</h2><p>Ao decidir instalar uma porta de vidro na sua casa ou em qualquer outro espaço, o primeiro passo é procurar uma vidraçaria, desde o desenvolvimento do projeto até a instalação na sua casa, é a empresa de vidraças que faz todo esse processo. E para garantir a qualidade desde o vidro, até na instalação, é imprescindível escolher uma vidraçaria confiável, que conta com uma equipe capacitada de montadores, e que esteja de acordo com as normas técnicas no16.259/2014 da Associação Brasileira de Normas Técnicas (ABNT).</p>

                    </article>
                <?php include('inc/coluna-lateral.php'); ?>
                <br class="clear" />
                <?php include('inc/paginas-relacionadas.php'); ?>
                <?php include('inc/regioes.php'); ?>
                <br class="clear">
                <?php include('inc/copyright.php'); ?>
            </div>
        </div>    
    <?php include('inc/footer.php'); ?>
</body>
</html>