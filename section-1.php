<section class="px-1 my-5">
	
    <div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-5 mt-3 col-12 text-center text-destaq text-black" style="color:#000">Conheça nossos produtos</h2>
				<span class="line-yellow my-2"></span>
			</div>
		</div>
		<p class="text-center">Escolha empresas que consigam atender a suas solicitações de maneira rápida e eficiente</p>
	</div>
	<div class="card-deck m-0 mt-3">
		<div class="container">
			<div class="justify-content-around owl-carousel">
            <?php  
                $palavraEstudo_s7 = array(
                'vidraçaria zona leste',
                'vidraçaria sp',
                'vidraçaria zona sul',
                'vidraçaria zona norte',
                'vidraçaria osasco',
                'vidraçaria guarulhos',
                'vidraçaria em campinas',
                'vidraçaria sorocaba'
            );
                
            include 'inc/vetKey.php';            
            asort($vetKey);
            foreach ($vetKey as $key => $value) {
                
            if(in_array(strtolower($value['key']), $palavraEstudo_s7)){
                
                $arquivojpg=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi/thumbs".DIRECTORY_SEPARATOR.$value['url']."-1.jpg";
                $arquivojpg0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi/thumbs".DIRECTORY_SEPARATOR.$value['url']."-01.jpg";
                $arquivopng=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi/thumbs".DIRECTORY_SEPARATOR.$value['url']."-1.png";
                $arquivopng0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi/thumbs".DIRECTORY_SEPARATOR.$value['url']."-01.png";

                if (file_exists($arquivojpg)) {
                    $imagem=$url."assets/img/img-mpi/thumbs/".$value['url']."-1.jpg"; 
                } else
                if (file_exists($arquivojpg0)) {
                    $imagem=$url."assets/img/img-mpi/thumbs/".$value['url']."-01.jpg";
                } else
                if (file_exists($arquivopng)) {
                    $imagem=$url."assets/img/img-mpi/thumbs/".$value['url']."-1.png";
                } else
                if (file_exists($arquivopng0)) {
                    $imagem=$url."assets/img/img-mpi/thumbs/".$value['url']."-01.png";
                } else {
                    $imagem=$url."assets/img/logo-ok.png";                        
                } 
          
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url.$value["url"]);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "qualDesc=1");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close ($ch);
 
				echo "<div class='col-12 p-0' style=''>
						<a href=\"".$url.$value["url"]."\" title=\"".$value['key']."\">
                                <div class=\" cards border\">
                                    <img class=\"card-img-top new-card \" src=\"".$imagem."\" alt=\"".$value['key']."\" title=\"".$value['key']."\">
                                    
                                    <div class=\"card-footer \">
                                    <h2 style='height:35px'>".$value['key']."</h2>
                                    <p  class=\"text-card m-0 p-0\" >".mb_strcut(strip_tags($server_output), 0, 57)."...</p>
                                </div>
                            </div>
                            </a>
                        </div>";
				
                } } ?>
			</div>
		</div>
	</div>
</section>