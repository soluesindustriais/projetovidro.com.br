<?php
include('inc/vetKey.php');
$h1 = "porta com vidro";
$title = $h1;
$desc = "Por que utilizar uma porta com vidro? Levando modernidade e elegância para os ambientes, o vidro é um material bastante utilizado em muitos projetos";
$key = "porta,com,vidro";
$legendaImagem = "Foto ilustrativa de porta com vidro";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=$url?>assets/css/mpi-style.css">
</head>

<body>
    <?php include 'inc/header.php' ?>   
        <div class="container">
            <div class="row">
                <article class="col-md-9 col-12">
                    <?php $quantia = 3;
                    include('inc/gallery.php'); ?>

                    <h2>Por que utilizar uma porta com vidro?</h2><p>Levando modernidade e elegância para os ambientes, o vidro é um material bastante utilizado em muitos projetos de arquitetura e decoração. Para promover a integração entre os cômodos de uma casa ou estabelecimento comercial, por exemplo, é possível utilizar uma porta com vidro, que permite a circulação do ar e da luza natural, além de trazer beleza e segurança para o local onde está instalada.</p><p>Além de permitir uma integração natural entre os ambientes, a utilização de uma porta de vidro pode ser combinada com outros materiais e revestimentos, como é o caso da madeira, do alumínio, do aço, do ferro, entre outros, facilitando a sua adequação em diferentes estilos de ambiente e decoração. Esse tipo de porta pode interligar tanto ambientes externos, quanto ambientes internos e pode ser encontrado em vidraçarias e lojas especializadas.</p><h2>Tipos de porta com vidro</h2><p>As portas são consideradas, muitas vezes, um item de segurança, porque evitam a entrada de agentes externos e criminosos. Por esse motivo, o material utilizado na composição de uma porta com vidro deve ser resistente contra impactos e evitar acidentes ocasionados pelos estilhaços que se formam em casos de quebras. Entre os tipos de vidro mais resistentes, é possível citar os vidros laminados e os vidros temperados, considerados vidros de segurança.</p><p>No mercado, é possível personalizar a confecção da porta com vidro. Os modelos mais conhecidos desse tipo de porta são:</p><ul><li>Porta de vidro de abrir;</li><li>Porta de vidro de correr;</li><li>Porta de vidro sanfonada;</li><li>Porta com abertura de vidro.</li></ul><p>No último caso, a porta com vidro possui apenas uma espécie de janela de vidro, e a sua estrutura pode ser confeccionada com aço ou alumínio, por exemplo. Nos outros casos, a porta é feita quase que exclusivamente de vidro, mas podem possuir uma moldura ou acabamento feito com outros tipos de materiais. Nesses casos, o preço da porta pode ser mais alto do que o de outros modelos e materiais.</p><h2>Como escolher a porta com vidro</h2><p>Para escolher o modelo mais adequado de porta com vidro é preciso avaliar a necessidade do cliente e o espaço disponível para a instalação da estrutura. Por isso, contar com o auxílio de um profissional especializado em arquitetura ou decoração pode ser uma boa alternativa. Além disso, é possível encontrar orientação especializada nas próprias vidraçarias que, hoje em dia, possuem equipes multidisciplinares de profissionais.</p>

                    </article>
                <?php include('inc/coluna-lateral.php'); ?>
                <br class="clear" />
                <?php include('inc/paginas-relacionadas.php'); ?>
                <?php include('inc/regioes.php'); ?>
                <br class="clear">
                <?php include('inc/copyright.php'); ?>
            </div>
        </div>    
    <?php include('inc/footer.php'); ?>
</body>
</html>