<?php
$h1 = "Quem Somos";
$title = $h1;
$desc = "O site foi pensado com o intuito de atender o mercado de vidros, espelho e vidraçarias. Ajudando assim, a busca do comprador encontrando o fornecedor que mais se enquadra em sua necessidade...";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include('inc/head.php'); ?>
</head>

<body>
    <?php include 'inc/header.php' ?>
	<section class="second-bloc py-5" style="margin-top:-3rem">
		<div class="container">

            

                
				<div class="row">
					
					<div class="col-md-12 mb-4">
                     <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-sec-block m-0 mb-4 text-center">Qualidade e comprometimento</h2>
                        <p class="text-justify">
                             O site foi pensado com o intuito de atender o mercado de vidros, espelho e vidraçarias. Ajudando assim, a busca do comprador encontrando o fornecedor que mais se enquadra em sua necessidade. Esse site faz parte do Soluções industriais, uma plataforma B2B focada em geração de novos negócios. Facilitando o contato entre as industriais e os seus clientes em potencial.
					       </p>
                        </div>
                    </div>
					
            </div>
							
				<div class="row">
                    
					<div class="col-md-4 mb-3">
                        <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-center">MISSÃO</h2>
						<p class="font-quem-somos text-center">
							O Soluções Industriais busca o crescimento por via da inovação e da qualidade contínua de seus processos e produtos. Satisfazer as necessidades dos nossos clientes, é o nosso maior foco e com o desenvolvimento sustentado da sociedade.
						</p>
                        </div>
					</div>
                    
					<div class="col-md-4 mb-3">
                        <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-center">VISÃO</h2>
						<p class="font-quem-somos text-center">
                            Ser reconhecido como maior portal de cotações da América Latina, atendendo as solicitações dos nossos clientes com qualidade e excelência. Tudo foi pensado para tornar a busca por produtos ou serviços industriais rápida e eficiente.</p>
                        </div>
                    </div>
                    
						<div class="col-md-4 mb-3" > 
                            <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                            <h2 class="text-center text-center">VALORES</h2>							
                                <ul><li>Ética;</li>
<li>Responsabilidade social;</li>
<li>Integridade;</li>
<li>Satisfação do cliente;</li>
<li>Honestidade;</li>
<li>Sinceridade.</li>

</ul>
                            
                            </div>
						</div>
					</div>
					
						
				<p class="w-100 text-center">
				A tecnologia Vidraçaria Ideal® é uma patente da empresa Soluções Industriais®, integrante do Grupo Ideal Trends®
				</p>
						
				
        </div>
				</section>
			

    <?php include 'inc/footer.php' ?>
</body>

</html>
