<article>
  <div class="prod-inc htmlchars">
    <?php
    $Read->ExeRead(TB_PRODUTO, "WHERE user_empresa = :emp AND prod_status = :stats AND prod_name = :nm", "emp=" . EMPRESA_CLIENTE . "&stats=2&nm={$lastCategory}");
    if (!$Read->getResult()):
      WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
    else:
      foreach ($Read->getResult() as $dados):
        extract($dados); ?>
        <div class="container">
          <div class="row">

            <!-- PRODUTO COVER / GALERIA -->
            <div class="p-2 col-6 col-md-12">
              <? include('inc/produto-inc-gallery.php'); ?>
            </div>

            <div class="p-2 col-6 col-md-12">
              <!-- PRODUTO SUBTÍTULO  -->
              <?php if(!empty($prod_codigo) && $prod_codigo != ' '): ?>
                <h2 class="text-center">Ref: <?=$prod_codigo?> - <?php Check::SetTitulo($arrBreadcrump, $URL); ?></h2>
                <?php if (PROD_BREVEDESC): ?>
                  <?=$prod_brevedescription?>
                <?php endif ?>
              <? else: ?>
                <h2 class="text-center">Confira - <?php Check::SetTitulo($arrBreadcrump, $URL); ?></h2>
                <?php if (PROD_BREVEDESC): ?>
                  <?=$prod_brevedescription?>
                <?php endif ?>
              <? endif; ?>

              <!-- CARRINHO -->
              <?php if (WIDGET_CART): ?>
                <div class="prod-inc-cart">
                  <?php include('inc/produto-inc-cart.php'); ?>
                </div>
              <?php endif; ?>
            </div>

            <!-- CONTEÚDO DESCRITIVO / DOWNLOADS -->
            <div class="p-2 col-12">
             <? include('inc/produto-inc-content.php'); ?>
           </div>

         </div> <!-- row -->
       </div> <!-- container -->
       <?php
     endforeach;
   endif; ?>
 </div> <!-- htmlchars -->
</article>
<?php include('inc/aside.php'); ?>
<? include('inc/itens-relacionados-inc.php'); ?>