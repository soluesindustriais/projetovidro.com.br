<?php if (WD_BANNER): ?>
  <?php
  $Read->ExeRead(TB_BANNER, "WHERE sl_status = :stats ORDER BY sl_id ASC", "stats=2");
  if ($Read->getResult()): ?>
    <div class="slick-banner hide-mobile">
      <?php
      foreach ($Read->getResult() as $dados):
        extract($dados); 
          if($dados['sl_url']): ?>          
              <a class="slick-banner__item" href="<?= RAIZ . '/' . $sl_url; ?>" title="<?= $sl_title; ?>">
                <img src="<?= RAIZ; ?>/doutor/uploads/<?= $sl_file; ?>" title="<?= $sl_title; ?>" alt="Imagem ilustrativa de <?= $sl_title; ?>" />
                <?php if($dados['sl_description']): ?>
                  <div class="slick-banner__overlay">
                    <div class="slick-banner__inner">
                      <h2 class="slick-banner__title"><?= $sl_title; ?></h2>
                      <p class="slick-banner__text"><?= $sl_description; ?></p>
                    </div>
                  </div>
                <?php endif; ?>
              </a>
          <?php else: ?>
              <div class="slick-banner__item">
                <img src="<?= RAIZ; ?>/doutor/uploads/<?= $sl_file; ?>" title="<?= $sl_title; ?>" alt="Imagem ilustrativa de <?= $sl_title; ?>" />
                <?php if($dados['sl_description']): ?>
                  <div class="slick-banner__overlay">
                    <div class="slick-banner__inner">
                      <h2 class="slick-banner__title"><?= $sl_title; ?></h2>
                      <p class="slick-banner__text"><?= $sl_description; ?></p>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
          <?php endif; 
        endforeach; ?>
    </div>
  <? endif;
endif; ?>