<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 5;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
    <div class="page-title">
      <div class="title col-md-12 col-sm-6 col-xs-12">
        <h3><i class="fa fa-cogs"></i> Configurações do sistema</h3>                
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">  
          <div class="pull-right">
            <button type="submit" name="UpdateConfig" class="btn btn-primary"><i class="fa fa-save"></i></button>            
          </div>
          <div class="clearfix"></div> 
          <br/>
          <?php
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['UpdateConfig'])):
            unset($post['UpdateConfig']);
            var_dump($post);
//            $update = new AdminConfig;
//            $update->ExeUpdate($get, $post);
//            $erro = $update->getError();
//            if (!$update->getResult()):
//              WSErro($erro[0], $erro[1], null, $erro[2]);
//            else:
//              WSErro($erro[0], $erro[1], null, $erro[2]);
//            endif;
          endif;

          $ReadConf = new Read;
          $ReadConf->ExeRead(TB_CONFIG);
          if (!$ReadConf->getResult() && isset($get)):
            WSErro("Não foi possível carregar as configurações do sistema.", WS_ALERT, null, "MPI Technology");
          else:
            $extracao = $ReadConf->getResult();
            ?>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">

                  <div class="x_title">                                        
                    <h2>Configurações gerais do Sistema</h2>
                    <div class="clearfix"></div>
                    <p>Atenção ao alterar as configurações do sistema com o site publicado.</p>
                    <div class="clearfix"></div>
                  </div>

                  <br/>
                  <?php
                  foreach ($extracao[0] as $key => $value):
                    if ($key != 'config_id' && $key != 'config_APP_USERS'):
                      ?>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?= $key; ?>"><?= str_replace('config_', 'Configuração: ', $key); ?></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input name="<?= $key; ?>" id="<?= $key; ?>" type="text" class="form-control" value="<?php
                          if (isset($post[$key])): echo $post[$key];
                          else: echo $value;
                          endif;
                          ?>">
                        </div>  
                      </div> 
                      <?php
                    endif;
                  endforeach;
                  ?>

                </div>
              </div>

            </div>     
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <button type="submit" name="UpdateConfig" class="btn btn-primary"><i class="fa fa-save"></i></button>      
    </div>
    <div class="clearfix"></div>
    <br/>
  </form>
</div>